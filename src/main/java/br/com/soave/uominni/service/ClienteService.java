/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.service;

import java.util.List;

import br.com.sarradanoar.uominni.uominni.model.Cliente;

/**
 * @author atrpa
 *
 */
public interface ClienteService {
	List<Cliente> obterCLientes();
	Cliente obterClienteCpf(String cpfCliente);
	boolean incluirCliente(Cliente cliente);
	void atualizarCliente(Cliente cliente);
	void excluirCliente(String cpfCliente);
}