/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.sarradanoar.uominni.uominni.dao.ClienteDao;
import br.com.sarradanoar.uominni.uominni.model.Cliente;

/**
 * @author atrpa
 *
 */
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteDao clienteDao;
	
	
	@Override
	public List<Cliente> obterCLientes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente obterClienteCpf(String cpfCliente) {
		// TODO Auto-generated method stub
		return clienteDao.obterClienteCpf(cpfCliente);
	}

	@Override
	public synchronized boolean incluirCliente(Cliente cliente) {
		if (clienteDao.clienteExistente(cliente.getNome(), cliente.getCpf())) {
			return false;
		}else {
			clienteDao.incluirCliente(cliente);
			return true;
		}
	}

	@Override
	public void atualizarCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void excluirCliente(String cpfCliente) {
		// TODO Auto-generated method stub
		
	}

}
