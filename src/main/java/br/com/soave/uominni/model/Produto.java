/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author atrpa
 *
 */
@Entity
@Table(name="TB_PRODUTO")
public class Produto {
    @Id
    @Column(name = "codigo", length = 45)
    @JsonProperty("codigo-produto")
    private String codigo;

    @JsonProperty("codigo-categoria")
    @ManyToOne
    @JoinColumn(name = "codigo_categoria", nullable = false)
    private Categoria categoria;
	
    @JsonProperty("nome-produto")
    @Column(name = "nome", length = 45)
    private String nome;

    @JsonProperty("descricao-produto")
    @Column(name = "descricao", length = 45)
    private String descricao;

    @JsonProperty("preco-custo-produto")
    @Column(name = "custo")
    private double custo;
    
    @JsonProperty("preco-venda-produto")
    @Column(name = "venda")
    private double venda;

    @JsonProperty("quantidade-estoque-produto")
    @Column(name = "estoque")
    private int estoque;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getCusto() {
        return custo;
    }

    public void setCusto(double custo) {
        this.custo = custo;
    }

    public double getVenda() {
        return venda;
    }

    public void setVenda(double venda) {
        this.venda = venda;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }
}