/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author atrpa
 *
 */
@Entity
@Table(name="TB_CATEGORIA")
public class Categoria {
    @Id
    @GeneratedValue
    private long codigo;

    @Column(name = "descricao", length=45)
    private String descricao;

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}