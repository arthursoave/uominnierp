/**
 * 
 */
package br.com.soave.uominni.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author soave
 */
@Entity
@Table(name="TB_CLIENTE")
public class Cliente {
    @Id
    @GeneratedValue
    @JsonProperty("codigo-cliente")
    private long codigo;
	
    @JsonProperty("nome-cliente")
    @Column(name = "nome", length = 45)
    private String nome;

    @JsonProperty("endereco-cliente")
    @Column(name = "endereco", length = 100)
    private String endereco;

    @JsonProperty("celular-cliente")
    @Column(name = "celular", length = 45)
    private String celular;
    
    @JsonProperty("telefone-cliente")
    @Column(name = "telefone", length = 45)
    private String telefone;
    
    @JsonProperty("limite-cliente")
    @Column(name = "limite")
    private double limite;

    @JsonProperty("cpf-cliente")
    @Column(name = "cpf", length = 45)
    private String cpf;

    @JsonProperty("status-cliente")
    @Column(name = "status", length = 45)
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}