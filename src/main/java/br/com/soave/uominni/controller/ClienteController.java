/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sarradanoar.uominni.uominni.model.Cliente;
import br.com.sarradanoar.uominni.uominni.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author atrpa
 *
 */

@RestController
public class ClienteController {

	@Autowired
	ClienteService clienteService;
	
	
	@ApiOperation(value = "Obter cliente atrav�s do nome")
	@RequestMapping(method = RequestMethod.GET, path = "/obterClienteNome", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Cliente obtido com sucesso", response = Cliente.class),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Cliente n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public Cliente obterClientePorNome(@RequestParam(value = "nomeCliente") String nomeCliente) {
		//TODO - Buscar cliente pelo nome	
		return new Cliente();
	}
	
	@ApiOperation(value = "Obter cliente atrav�s do cpf")
	@RequestMapping(method = RequestMethod.GET, path = "/obterClienteCpf", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Cliente obtido com sucesso", response = Cliente.class),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Cliente n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public Cliente obterClientePorCpf(@RequestParam(value = "cpfCliente") String cpfCliente) {
		//TODO - Buscar cliente pelo cpf	
		return clienteService.obterClienteCpf(cpfCliente);
		//return new Cliente();
	}

	@ApiOperation(value = "Incluir novo Cliente")
	@RequestMapping(method = RequestMethod.POST, path = "/incluirCliente", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente incluido com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Cliente n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public boolean inserirCliente(@RequestBody Cliente cliente) {
		//TODO - Inclus�o do Cliente
		return clienteService.incluirCliente(cliente); 
	}

	@ApiOperation(value = "Alterar dados do Cliente")
	@RequestMapping(method = RequestMethod.PUT, path = "/alterarCliente", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente alterado com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Cliente n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public Cliente atualizarCliente(@RequestBody Cliente cliente) {
		//TODO - Alterar dados do cliente
		return new Cliente();
	}

	@ApiOperation(value = "Excluir dados do Cliente")
	@RequestMapping(method = RequestMethod.DELETE, path = "/excluirCliente", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente excluido com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Cliente n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public void deletarCliente(@RequestParam(value = "cpfCliente") String cpfCliente) {
		//TODO - Excluir Cliente
	}
	
}
