/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sarradanoar.uominni.uominni.model.Produto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author atrpa
 *
 */

@RestController
public class ProdutoController {

	@ApiOperation(value = "Incluir novo Produto")
	@RequestMapping(method = RequestMethod.POST, path = "/incluirProduto", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Produto incluido com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public void inserirProduto(@RequestBody Produto produto) {
		//TODO - Incluir produto
	}	
	
	@ApiOperation(value = "Alterar um Produto")
	@RequestMapping(method = RequestMethod.PUT, path = "/alterarProduto", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Produto alterado com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Produto n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public Produto alterarProduto(@RequestBody Produto produto) {
		//TODO - Altera��o dos produtos
		return new Produto();
	}
	
	@ApiOperation(value = "Excluir um Produto")
	@RequestMapping(method = RequestMethod.DELETE, path = "/excluirProduto", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Produto exclu�do com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Produto n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public void deletarProduto(@RequestParam int codigoProduto) {
		//TODO - Excluir produto
	}
	
	@ApiOperation(value = "Obter um Produto pelo c�digo")
	@RequestMapping(method = RequestMethod.GET, path = "/obterProdutoCodigo", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto obtido com sucesso", response = Produto.class),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Produto n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})	
	public Produto obterProdutoCodigo(@RequestParam int codigoProduto) {
		//TODO - Obter produto atrav�s do c�digo
		return new Produto();
	}
	
	@ApiOperation(value = "Obter um Produto pelo nome")
	@RequestMapping(method = RequestMethod.GET, path = "/obterProdutoNome", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto obtido com sucesso", response = Produto.class),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Produto n�o encontrado"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})	
	public Produto obterProdutoNome(@RequestParam String nomeProduto) {
		//TODO - Obter produto atrav�s do c�digo
		return new Produto();
	}	
}
