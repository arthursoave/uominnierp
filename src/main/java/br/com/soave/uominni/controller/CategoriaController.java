/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sarradanoar.uominni.uominni.model.Categoria;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author atrpa
 *
 */

@RestController
public class CategoriaController {
	
	@ApiOperation(value = "Incluir nova Categoria")
	@RequestMapping(method = RequestMethod.POST, path = "/incluirCategoria", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Categoria incluida com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public void incluirCategoria(@RequestBody Categoria categoria) {
		//TODO - Incluir categoria
	}

	@ApiOperation(value = "Alterar Categoria")
	@RequestMapping(method = RequestMethod.PUT, path = "/alterarCategoria", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Categoria alterada com sucesso"),
			@ApiResponse(code = 401, message = "Opera��o n�o autorizada"),
			@ApiResponse(code = 403, message = "Voc� n�o possui permiss�o para esta opera��o"),
			@ApiResponse(code = 404, message = "Categoria n�o encontrada"),
			@ApiResponse(code = 500, message = "Falha de comunica��o com o servidor da aplica��o")})
	public Categoria alterarCategoria(@RequestBody Categoria categoria) {
		return new Categoria();
	}
	
}
