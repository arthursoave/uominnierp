package br.com.sarradanoar.uominni.uominni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.sarradanoar.uominni.uominni.service.ClienteServiceImpl;

@SpringBootApplication
public class UominniApplication {
	
	//teste git
	public static void main(String[] args) {
		SpringApplication.run(UominniApplication.class, args);
	}
	
	@Bean
	public ClienteServiceImpl clienteService() {
		return new ClienteServiceImpl();
	}
}
