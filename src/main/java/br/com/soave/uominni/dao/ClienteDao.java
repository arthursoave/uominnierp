package br.com.sarradanoar.uominni.uominni.dao;

import java.util.List;

import br.com.sarradanoar.uominni.uominni.model.Cliente;

public interface ClienteDao {
	
	List<Cliente> obterTodosClientes();
	Cliente obterClienteCpf(String cpfCliente);
	Cliente obterClienteNome(String nomeCliente);
	void incluirCliente(Cliente cliente);
	void alterarCliente(Cliente cliente);
	void excluirCliente(int codigoCliente);
	boolean clienteExistente(String nomeCsliente, String string);
	

}
