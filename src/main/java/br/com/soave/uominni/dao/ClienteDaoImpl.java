/**
 * 
 */
package br.com.sarradanoar.uominni.uominni.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sarradanoar.uominni.uominni.model.Cliente;

/**
 * @author atrpa
 *
 */

@Transactional
@Repository
public class ClienteDaoImpl implements ClienteDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Cliente> obterTodosClientes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente obterClienteCpf(String cpfCliente) {
		return entityManager.find(Cliente.class, cpfCliente);
	}

	@Override
	public Cliente obterClienteNome(String nomeCliente) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void incluirCliente(Cliente cliente) {
		entityManager.persist(cliente);
		
	}

	@Override
	public void alterarCliente(Cliente cliente) {
		Cliente clie = obterClienteCpf(cliente.getCpf());
		entityManager.flush();
		
	}

	@Override
	public void excluirCliente(int codigoCliente) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean clienteExistente(String nomeCliente, String cpfCliente) {
		// TODO Auto-generated method stub
		return false;
	}
}
